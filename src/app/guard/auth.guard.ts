import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot,
    UrlTree
} from "@angular/router";
import { AppService } from "../app.service";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private AppService: AppService,
        private router: Router) { }
    canActivate(): boolean {
        if (!this.getToken()) {
            this.router.navigateByUrl('/login');
            return false;
        }
        return true;
    }
    public getToken(): string {
        return localStorage.getItem('token');
    }
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRouterModule } from './login-router.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRouterModule
  ]
})
export class LoginModule { }

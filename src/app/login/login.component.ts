import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { User } from '../store/user';
import { Login } from '../store/auth.actions';
import { IAppState, selectAuthState } from '../app.states';
import { IState } from '../store/auth.reducer';
import { Observable } from 'rxjs';
import { AppService } from "../app.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();
  loginForm: FormGroup;
  state: Observable<any>;
  error: any;
  isLoading: boolean;
  hide = true;

  constructor(private fb: FormBuilder, private store: Store<IAppState>, private AppService: AppService) {
    this.state = this.store.select(selectAuthState);
  }

  ngOnInit() {
    this.state.subscribe((s: IState) => {
      this.error = s.error
      this.isLoading = s.isLoading
    })
    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]),
      password: new FormControl('', [Validators.required]),
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.loginForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) &&
      (control.dirty || control.touched);
    return result;
  }


  login() {

    this.store.dispatch(new Login(this.loginForm.value));

  }



}
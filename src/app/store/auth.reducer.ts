import { User } from "./user";
import { AuthActionTypes } from "./auth.actions";

export interface IState {
  isAuthenticated: boolean;
  isLoading: boolean,
  user: User | null;
  error: any;
}

export const initialState = {
  isAuthenticated: false,
  isLoading: false,
  user: null,
  error: null
};

export function reducer(state = initialState, action: any): IState {
  console.log(action)
  switch (action.type) {
    case AuthActionTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        user: {
          email: action.payload.email,
          password: action.payload.password
        },
        error: null
      };
    }


    case AuthActionTypes.LOADING_SHOW: {
      return {
        ...state,
        isLoading: action.payload.isLoading
      };
    }
    case AuthActionTypes.LOADING_HIDE: {
      return {
        ...state,
        isLoading: action.payload.isLoading
      };
    }

    case AuthActionTypes.LOGIN_ERROR: {
      return {
        ...state,
        error: 'Invalid Email/Password'
      };
    }

    case AuthActionTypes.LOGOUT: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}



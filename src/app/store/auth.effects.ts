import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from '@ngrx/store';
import { IAppState, selectAuthState } from '../app.states';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from "rxjs";
import { catchError, map, switchMap, tap } from "rxjs/operators";
import { AppService } from "../app.service";
import { AuthActionTypes, Login, LoadingShow, LoadingHide, LoginSuccess, LoginFailure } from "./auth.actions";

@Injectable()
export class AuthEffects {

    state: Observable<any>;

    @Effect()
    LoginIn: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN),
        map((action: Login) => action.payload),
        switchMap(payload => {
            this.store.dispatch(new LoadingShow({ isLoading: true }))
            return this.authService.get(`https://62e4c71820afdf238d71eaca.mockapi.io/api/v1/users`).pipe(
                map((user: any) => {
                    let result = user.filter(data => { return data.Email == payload.email && data.Password == payload.password });
                    try {
                        if (result.length > 0) {
                            return new LoginSuccess({ email: payload.email, password: payload.password });
                        } {
                            throw new Error('User not found')
                        }
                    } catch (e) {
                        throw new Error(e)
                    }

                }),
                catchError((error) => {
                    return of(new LoginFailure({ error }));
                })
            )
        })
    )


    @Effect({ dispatch: false })
    LoginSuccess: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_SUCCESS),
        tap((user) => {
            this.store.dispatch(new LoadingHide({ isLoading: false }))
            localStorage.setItem('token', user.payload.email);
            this.router.navigate(['tender/home']);
        })
    )

    @Effect({ dispatch: false })
    LogInFailure: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_ERROR),
        tap((user) => {
            this.store.dispatch(new LoadingHide({ isLoading: false }))
        })
    );

    @Effect({ dispatch: false })
    LogOut: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGOUT),
        tap(() => {
            localStorage.removeItem('token');
            this.router.navigate(['login']);
        })
    )



    constructor(
        private actions: Actions,
        private authService: AppService,
        private router: Router,
        private store: Store<IAppState>
    ) {
        this.state = this.store.select(selectAuthState);
    }
}
import { Action } from "@ngrx/store";

export enum AuthActionTypes {
    LOGIN = '[Auth] Login',
    LOADING_SHOW = '[Auth] Submit Login',
    LOADING_HIDE = '[Auth] Submit Login',
    LOGIN_SUCCESS = '[Auth] SUCCESS',
    LOGIN_ERROR = "[Auth] LOGIN_ERROR",
    LOGOUT = "[Auth] LOGOUT",
    LOADED = '[Auth] LOAD USER'
}

export class Login implements Action {
    type = AuthActionTypes.LOGIN;
    constructor(public payload: any) { }

}


export class LoadingShow implements Action {
    type = AuthActionTypes.LOADING_SHOW;
    constructor(public payload: any) { }

}
export class LoadingHide implements Action {
    type = AuthActionTypes.LOADING_HIDE;
    constructor(public payload: any) { }

}

export class LoginSuccess implements Action {
    type = AuthActionTypes.LOGIN_SUCCESS;
    constructor(public payload: any) {

    }
}

export class LoginFailure implements Action {
    type = AuthActionTypes.LOGIN_ERROR;
    constructor(public payload: any) { }

}

export class Logout implements Action {
    type = AuthActionTypes.LOGOUT
}



export type All =
    | Login
    | LoginSuccess
    | LoginFailure
    | Logout

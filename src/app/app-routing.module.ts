import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import {
  AuthGuard
} from './guard/auth.guard';
const routes: Routes = [

  {
    path: 'tender',
    canActivate: [AuthGuard],
    loadChildren: () => import('./tender/tender.module').then(m => m.TenderModule)
  },

  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },

  {
    path: 'error',
    component: NotFoundComponent
  },
  { path: '', redirectTo: 'tender', pathMatch: 'full' },
  { path: '**', redirectTo: 'error', pathMatch: 'full' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

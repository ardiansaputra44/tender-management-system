import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class AppService {
  constructor(private http: HttpClient) { }

  get(endPoint): Observable<any> {
    return this.http.get<any>(endPoint).pipe(
      map((res) => {
        return res;
      }),
      catchError((err) => {
        console.error('Error', err);
        return of({
          status: false,
          message: err,
          data: null,
        });
      })
    );
  }
  post(endPoint, data): Observable<any> {
    return this.http.post<any>(endPoint, data).pipe(
      map((res) => {
        return res;
      }),
      catchError((err) => {
        console.error('Error', err);
        return of({
          status: false,
          message: err,
          data: null,
        });
      })
    );
  }

  put(endPoint, id): Observable<any> {
    return this.http.put<any>(endPoint, id).pipe(
      map((res) => {
        return res;
      }),
      catchError((err) => {
        console.error('Error', err);
        return of({
          status: false,
          message: err,
          data: null,
        });
      })
    );
  }

  delete(endPoint): Observable<any> {
    return this.http.delete<any>(endPoint).pipe(
      map((res) => {
        return res;
      }),
      catchError((err) => {
        console.error('Error', err);
        return of({
          status: false,
          message: err,
          data: null,
        });
      })
    );
  }
}



import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../app.states';
import { Logout } from '../store/auth.actions';
@Component({
  selector: 'app-tender',
  templateUrl: './tender.component.html',
  styleUrls: ['./tender.component.css']
})
export class TenderComponent implements OnInit {

  constructor(private store: Store<IAppState>) { }
  Hello = localStorage.getItem('token');
  ngOnInit(): void {
  }

  logout() {

    this.store.dispatch(new Logout());

  }

}

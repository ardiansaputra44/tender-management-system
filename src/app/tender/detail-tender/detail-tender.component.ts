import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
export interface Data {
  tender?: [],
}

@Component({
  selector: 'app-detail-tender',
  templateUrl: './detail-tender.component.html',
  styleUrls: ['./detail-tender.component.css']
})
export class DetailTenderComponent implements OnInit {
  row: any;
  constructor(
    public dialogRef: MatDialogRef<DetailTenderComponent>,
    @Inject(MAT_DIALOG_DATA) public refData: Data,
  ) { }

  ngOnInit(): void {
    this.row = this.refData
  }

  closeDialog() {
    this.dialogRef.close();
  }

}

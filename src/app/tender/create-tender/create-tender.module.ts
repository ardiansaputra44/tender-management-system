import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CreateTenderComponent } from './create-tender.component'
import { MaterialModule } from '.././../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
const routes: Routes = [

  {
    path: '',
    component: CreateTenderComponent,
  },

];

@NgModule({
  declarations: [CreateTenderComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CreateTenderModule { }

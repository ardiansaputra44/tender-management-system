import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  FormControl
} from '@angular/forms';
import { AppService } from '../../app.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-create-tender',
  templateUrl: './create-tender.component.html',
  styleUrls: ['./create-tender.component.css'],
})
export class CreateTenderComponent implements OnInit {
  idTender = this.route.snapshot.paramMap.get('id') ? this.route.snapshot.paramMap.get('id') : '';
  ReactiveFormTender: FormGroup;
  submit: boolean = false;
  isLoading: boolean = false;
  submitted: boolean = false;
  public todayDate: any = new Date();
  g: any;
  constructor(
    private FormBuilder: FormBuilder,
    private AppService: AppService,
    private router: Router,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef
  ) {
    this.initForm();
  }

  ngOnInit() {
    if (this.idTender !== '') this.getTenderById();
  }

  initForm() {
    this.ReactiveFormTender = this.FormBuilder.group({
      name: new FormControl('', [Validators.required]),
      reference: new FormControl('', [Validators.required]),
      releaseDate: new FormControl('', [Validators.required]),
      closingDate: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      idTender: new FormControl(''),
    },
      { validator: this.dateLessThan('releaseDate', 'closingDate') });


  }

  dateLessThan(from: string, to: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];

      if (t.value !== '' && f.value !== '' && t.value && f.value >= t.value) {
        return {
          dates: `Release Date from should be less than Closing Date`
        };
      }
      return {};
    }
  }

  public isTouchedAndInvalid(formControlName: AbstractControl): boolean {
    return formControlName.touched && formControlName.invalid;
  }


  async getTenderById() {
    this.isLoading = true;
    let response = await this.AppService.get(
      `https://62e4c71820afdf238d71eaca.mockapi.io/api/v1/tender/${this.idTender}`
    ).toPromise();

    let data = response;
    this.todayDate = data.releaseDate
    this.ReactiveFormTender.patchValue(data);
    this.isLoading = false;
    this.cdr.detectChanges()

  }

  async onSubmit() {
    this.submit = true;
    let response$: any;
    // when Tender ID Empty to Insert Data
    if (this.idTender == '') {
      response$ = this.AppService.post(
        `https://62e4c71820afdf238d71eaca.mockapi.io/api/v1/tender`,
        this.ReactiveFormTender.value
      );
    } else if (this.idTender !== '') {
      response$ = this.AppService.put(
        `https://62e4c71820afdf238d71eaca.mockapi.io/api/v1/tender/${this.idTender}`,
        this.ReactiveFormTender.value
      );
    }
    let response = await response$.toPromise();

    if (response) {
      this.submit = false;
      this.router.navigate(['/tender/list']);
    }

  }
}
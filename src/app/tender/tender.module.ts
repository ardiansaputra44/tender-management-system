import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TenderRoutingModule } from './tender-router.module';
import { TenderComponent } from './tender.component';
@NgModule({
  declarations: [TenderComponent],
  imports: [
    CommonModule,
    TenderRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class TenderModule { }

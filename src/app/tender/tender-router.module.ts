import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TenderComponent } from "./tender.component"

const routes: Routes = [

  {
    path: '',
    component: TenderComponent,
    children: [
      {
        path: 'home',
        loadChildren: () => import('./welcome/welcome.module').then(m => m.WelcomeModule)
      },
      {
        path: 'list',
        loadChildren: () => import('./list-tender/list-tender.module').then(m => m.ListTenderModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./create-tender/create-tender.module').then(m => m.CreateTenderModule)
      },
      {
        path: 'edit/:id',
        loadChildren: () => import('./create-tender/create-tender.module').then(m => m.CreateTenderModule)
      },
    ]
  },

  { path: '**', redirectTo: 'error', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TenderRoutingModule { }
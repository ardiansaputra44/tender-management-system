import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { AppService } from '../../app.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DetailTenderComponent, Data } from "../detail-tender/detail-tender.component"
import { Overlay } from '@angular/cdk/overlay';

export interface Article {
  id: number;
  title: string;
}

@Component({
  selector: 'app-list-tender',
  templateUrl: './list-tender.component.html',
  styleUrls: ['./list-tender.component.css']
})
export class ListTenderComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  MyDataSource: any;
  displayedColumns = ['TenderID', 'ContractNo', 'TenderName', 'ReleaseDate', 'ClosingDate', 'Action'];
  loading: boolean = false;
  submit: boolean = false;
  constructor(
    private AppService: AppService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    public overlay: Overlay,
  ) {

  }

  ngOnInit(): void {
    this.RenderDataTable()
  }



  RenderDataTable() {
    this.loading = true;
    this.AppService.get(`https://62e4c71820afdf238d71eaca.mockapi.io/api/v1/tender`)
      .subscribe(
        res => {

          this.MyDataSource = new MatTableDataSource();
          this.MyDataSource.data = res;
          this.MyDataSource.sort = this.sort;
          this.MyDataSource.paginator = this.paginator;
          this.loading = false;

        },
        error => {
          console.log('There was an error while retrieving Posts !!!' + error);
        });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.MyDataSource.filter = filterValue.trim().toLowerCase();
  }


  async onDelete(id) {


    let result = await Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'Cancel',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return this.AppService.delete(
          `https://62e4c71820afdf238d71eaca.mockapi.io/api/v1/tender/${id}`
        ).toPromise();
      },
      allowOutsideClick: () => !Swal.isLoading()
    })

    if (result.isConfirmed) {

      this.RenderDataTable();

    }

  }

  async DetailTender(ParamIDTender) {

    let ResultFilter = this.MyDataSource.data.filter(function (el) {
      return el.idTender == ParamIDTender;
    }
    );

    const dialogConfig = new MatDialogConfig();
    dialogConfig.hasBackdrop = true;
    dialogConfig.width = "900px";
    dialogConfig.panelClass = 'my-dialog',
      dialogConfig.scrollStrategy = this.overlay.scrollStrategies.noop();
    dialogConfig.data = { detail: Object.assign({}, ...ResultFilter) };
    this.dialog.open<DetailTenderComponent, Data>(DetailTenderComponent, dialogConfig);


  }


}


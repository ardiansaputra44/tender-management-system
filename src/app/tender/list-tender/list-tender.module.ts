import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '.././../../material.module';
import { Routes, RouterModule } from '@angular/router';
import { ListTenderComponent } from './list-tender.component'
const routes: Routes = [

  {
    path: '',
    component: ListTenderComponent,
  },

];


@NgModule({
  declarations: [ListTenderComponent],
  imports: [CommonModule, MaterialModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListTenderModule { }
